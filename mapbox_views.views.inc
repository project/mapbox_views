<?php

/**
 * @file
 * Contains mapbox_views\mapbox_views.views.inc..
 * Provide a custom views field data that isn't tied to any other module. */


/**
* Implements hook_views_data().
*/
function mapbox_views_views_data() {

    $data['views']['table']['group'] = t('Custom Global');
    $data['views']['table']['join'] = [
      // #global is a special flag which allows a table to appear all the time.
      '#global' => [],
    ];

    $data['views']['mapbox'] = [
        'title' => t('Mapbox'),
        'help' => t('My awesome custom views field plugin.'),
        'field' => [
            'id' => 'mapbox',
        ],
    ];
    return $data;
}
