<?php

namespace Drupal\mapbox_views\Plugin\views\style;

use Drupal\core\form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mapbox\Mapbox;

/**
 * Style plugin to render a list of years and months
 * in reverse chronological order linked to content.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "mapbox_style",
 *   title = @Translation("Mapbox - All in one"),
 *   help = @Translation("Renders all the markers on one mapbox map."),
 *   theme = "mapbox_views",
 *   display_types = { "normal" }
 * )
 */

class MapboxStyle extends StylePluginBase {
  protected $usesFields = TRUE;
  protected $usesRowPlugin = TRUE;
  protected $usesRowClass = FALSE;
  protected $usesGrouping = FALSE;

  /**
   * Drupal\mapbox\Mapbox definition.
   *
   * @var \Drupal\mapbox\Mapbox
   */
  protected $mapbox;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Mapbox $mapbox) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mapbox = $mapbox;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('mapbox')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['threeDBuildings'] = ['default' => 1];
    $options['keyboard']        = ['default' => 0];
    $options['navigation']      = ['default' => 0];
    $options['minZoom']         = ['default' => 3];
    $options['maxZoom']         = ['default' => 15];
    $options['maxPitch']        = ['default' => 65];
    $options['pitch']           = ['default' => 45];
    return $options;
  }
  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $fields = $this->displayHandler->getOption('fields');
    $labels = $this->displayHandler->getFieldLabels();
    $map_fields_options = [];
    foreach ($fields as $field_name => $field) {
      if ($field['type'] == 'mapbox_raw_formatter') {
        $map_fields_options[$field_name] = $labels[$field_name];
      }
    }
    $form['mapbox_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Mapbox Settings'),
      '#open' => TRUE
    ];
    $form['source_field'] = [
      '#type' => 'select',
      '#title' => t('Source Field'),
      '#fieldset' => 'mapbox_settings',
      '#options' => $map_fields_options,
      '#required' => TRUE,
      '#default_value' => (isset($this->options['source_field'])) ? $this->options['source_field'] : '',
      '#description' => t('Source Mapbox field. If the dropdown is empty there is a chance you haven\'t added a Mapbox field yet.'),
    ];
    $form['threeDBuildings'] = [
      '#title' => $this->t('3D Buildings'),
      '#type' => 'checkbox',
      '#fieldset' => 'mapbox_settings',
      '#default_value' => (isset($this->options['threeDBuildings'])) ? $this->options['threeDBuildings'] : 0,
    ];
    $form['keyboard'] = [
      '#title' => $this->t('Keyboard Controls'),
      '#type' => 'checkbox',
      '#fieldset' => 'mapbox_settings',
      '#default_value' => (isset($this->options['keyboard'])) ? $this->options['keyboard'] : 0,
    ];
    $form['navigation'] = [
      '#title' => $this->t('Navigation Controls'),
      '#type' => 'checkbox',
      '#fieldset' => 'mapbox_settings',
      '#default_value' => (isset($this->options['navigation'])) ? $this->options['navigation'] : 0,
    ];
    $form['minZoom'] = [
      '#title' => $this->t('Min Zoom'),
      '#type' => 'number',
      '#fieldset' => 'mapbox_settings',
      '#default_value' => (isset($this->options['minZoom'])) ? $this->options['minZoom'] : 3,
      '#attributes' => [
        'min' => 3,
        'max' => 18
      ]
    ];
    $form['maxZoom'] = [
      '#title' => $this->t('Max Zoom'),
      '#type' => 'number',
      '#fieldset' => 'mapbox_settings',
      '#default_value' => (isset($this->options['maxZoom'])) ? $this->options['maxZoom'] : 15,
      '#attributes' => [
        'min' => 5,
        'max' => 18
      ]
    ];
    $form['maxPitch'] = [
      '#title' => $this->t('Max Pitch'),
      '#type' => 'select',
      '#fieldset' => 'mapbox_settings',
      '#options' => [30 => 30, 35 => 35, 40 => 40, 45 => 45, 50 => 50, 55 => 55, 60 => 60, 65 => 65],
      '#default_value' => (isset($this->options['maxPitch'])) ? $this->options['maxPitch'] : 65,
    ];
    $form['pitch'] = [
      '#title' => $this->t('Default Pitch'),
      '#type' => 'select',
      '#fieldset' => 'mapbox_settings',
      '#options' => [30 => 30, 35 => 35, 40 => 40, 45 => 45, 50 => 50, 55 => 55, 60 => 60, 65 => 65],
      '#default_value' => (isset($this->options['pitch'])) ? $this->options['pitch'] : 45,
    ];
    $form['zoom'] = [
      '#title' => $this->t('Default Zoom'),
      '#type' => 'number',
      '#fieldset' => 'mapbox_settings',
      '#default_value' => (isset($this->options['zoom'])) ? $this->options['zoom'] : 9,
      '#attributes' => [
        'min' => 3,
        'max' => 18
      ]
    ];
  }

  public function render() {
    $source_field = $this->options['source_field'];

    foreach ($this->view->result as $row) {
      $mapbox_field = $this->view->field[$source_field];
      $mapbox_field_items = $mapbox_field->getItems($row);
      foreach ($mapbox_field_items as $delta => $item) {
        $mapbox = $item['raw'];
        $positions[] = [
          'lat' => (float) $mapbox->lat,
          'lng' => (float) $mapbox->lng,
          'marker' => $mapbox->marker ? $mapbox->marker : '/'.drupal_get_path('module', 'mapbox').'/marker.png',
          'markerText' => $mapbox->markerText ?? $mapbox->markerText,
          'markerInstance' => $this->view->dom_id . $delta . rand(1000000, 999999)
        ];
      }
    }

    $options = [
      'threeDBuildings' => (int) $this->options['threeDBuildings'],
      'navigation'      => (int) $this->options['navigation'],
      'keyboard'        => (int) $this->options['keyboard'],
      'maxPitch'        => (int) $this->options['maxPitch'],
      'maxZoom'         => (int) $this->options['maxZoom'],
      'minZoom'         => (int) $this->options['minZoom'],
      'pitch'           => (int) $this->options['pitch'],
      'zoom'            => (int) $this->options['zoom'],
    ];

    $mapbox_settings['mapboxInstance']  = 'mapbox-map-'.$this->view->dom_id;
    $mapbox_settings['accessToken'] = $this->mapbox->accessToken();
    $mapbox_settings['style']       = $this->mapbox->getStyle();
    $mapbox_settings['options']     = $options;
    $mapbox_settings['positions']   = $positions;
    $mapbox_settings['center']      = $positions[0];
    $build = [
      '#theme'  => 'mapbox_views',
      '#positions' => $mapbox_settings['positions'],
      '#mapboxInstance' => $mapbox_settings['mapboxInstance']
    ];

    $build['#attached']['library'][] = 'mapbox/singlemap';
    $build['#attached']['library'][] = 'mapbox/apis';
    $build['#attached']['drupalSettings']['mapbox_singlemap'] = $mapbox_settings;

    return $build;
  }
}
